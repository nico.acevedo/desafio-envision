import '../styles/table.css';

const Tabla = (props)=>{
    return(
        <div id="container">
        <table id="tabla">
            <tbody>
                <tr>
                    <th>N°</th>
                    <th>Título</th>
                    <th>Año</th>
                    <th>Director</th>
                    <th>Rating</th>
                    <th>Metascore</th>
                </tr>
                {
                    props.datos.map((peli, i)=>{
                        return(<tr key={i}>
                            <td>{i+1}</td>
                            <td>{peli.title}</td>
                            <td>{peli.year}</td>
                            <td>{peli.director}</td>
                            <td>{peli.rating}</td>
                            <td>{peli.metascore}</td>
                        </tr>)
                    })
                }
            </tbody>
        </table>
        </div>
    );
};

export default Tabla;