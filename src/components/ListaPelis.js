import React, { useState, useEffect } from 'react';
import axios from 'axios';

import '../styles/button.css';
import Tabla from './Tabla';

const ListaPelis = (props)=>{

    const getURL = "https://prod-61.westus.logic.azure.com/workflows/984d35048e064b61a0bf18ded384b6cf/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=6ZWKl4A16kST4vmDiWuEc94XI5CckbUH5gWqG-0gkAw"
    const postURL = "https://prod-62.westus.logic.azure.com/workflows/779069c026094a32bb8a18428b086b2c/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=o_zIF50Dd_EpozYSPSZ6cWB5BRQc3iERfgS0m-4gXUo"

    const [botonLoading, setBotonLoading] = useState(false);
    const [contenido, setContenido] = useState([]);

    const [ratingSorted, setRatingSorted] = useState([]);
    const [metascoreSorted, setMetascoreSorted] = useState([]);

    useEffect(()=>{
        if(ratingSorted!==0){
            setBotonLoading(false);
        }
    },[ratingSorted])

    useEffect(()=>{
        if(contenido.length!==0){

            const rs = [].concat(contenido).sort((a,b)=>
                a.rating < b.rating ? 1 : -1
            );
            setRatingSorted(rs)

            const ms = [].concat(contenido).sort((a,b)=>
            a.metascore < b.metascore ? 1 : -1
            ).map((e,i)=>e.title);
            setMetascoreSorted(ms);
        }
    },[contenido])

    useEffect(()=>{
        if(metascoreSorted.length!==0){

            const payload = {
                "RUT": "19499475-3",
                "Peliculas": metascoreSorted
            };
    
            axios.post(postURL,payload)
            .then(response=>{
                console.log(response.data.message);
            })
            .catch(err=>console.log(err));
        }
    },[metascoreSorted])

    const handleClick = ()=>{
        setBotonLoading(true);

        axios.get(getURL)
        .then(response=>{
            setContenido(response.data.response);
        })
        .catch(err=>console.log(err))
    }

    return(
        <div>
            <button id="button"
                onClick={()=>handleClick()}
            >
                {botonLoading ? 'Cargando...' : 'Generar Listado'}
            </button>
            {
                ratingSorted.length!==0
                ?
                <div>
                    <Tabla datos={ratingSorted}></Tabla>
                </div>
                :
                ""
            }
        </div>
    )
};

export default ListaPelis;