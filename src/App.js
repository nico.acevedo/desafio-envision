import './App.css';
import ListaPelis from './components/ListaPelis';

function App() {
  return (
    <div className="App">
      <p id="header">Top 10 mejores películas según IMDB</p>
      <ListaPelis></ListaPelis>
    </div>
  );
}

export default App;
