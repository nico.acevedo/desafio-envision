# Desafío Envision

Desafío para postular al área de ingeniería en Envision.

## Ejecución

Se debe ejecutar

### `npm install`

Esto instala todas las dependencias en `package-lock.json`. Luego,

### `npm start`

para ejecutar el programa en [`http://localhost:3000/`](http://localhost:3000/).
